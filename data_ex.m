# para correr el archivo usar 'soure data_ex.m' en octave-gui en windows
# script e imagenes tienen que estar en el mismo directorio
# se debe ingresar  el kernel 3x3 a usar

pkg load image;

##Pedir kernel y convertirlo a HEX
prompt = {"k1,1", "k1,2", "k1,3","k2,1", "k2,2", "k2,3","k3,1", "k3,2", "k3,3"};
defaults = {"0","0", "0", "0", "1", "0", "0", "0", "0"};
rowscols = [1; 1; 1; 1; 1; 1; 1; 1; 1;];
dims = inputdlg (prompt, "Kernel", rowscols, defaults);
k11=sdec2hex(str2num(dims{1,1}),8);
k21=sdec2hex(str2num(dims{2,1}),8);
k31=sdec2hex(str2num(dims{3,1}),8);
k41=sdec2hex(str2num(dims{4,1}),8);
k51=sdec2hex(str2num(dims{5,1}),8);
k61=sdec2hex(str2num(dims{6,1}),8);
k71=sdec2hex(str2num(dims{7,1}),8);
k81=sdec2hex(str2num(dims{8,1}),8);
k91=sdec2hex(str2num(dims{9,1}),8);

# I = imread ("FNAFs.jpg"); ## cambiar nombre de la imagen
[fn,pn] = uigetfile ({"*.png;*.jpg", "Supported Picture Formats"}); # seleccionar la imagen
I = imread (fullfile(pn,fn)); ## cambiar nombre de la imagen
I_GRAY = rgb2gray(I);
I_GRAY=padarray(I_GRAY,[1 1],"symmetric");
I_size = size(I_GRAY);

#abre el archivo
fid = fopen('Data_Image.di','wt');

# Guarda los valores del kernel
fprintf(fid,k11);
fprintf(fid,'\n');
fprintf(fid,k21);
fprintf(fid,'\n');
fprintf(fid,k31);
fprintf(fid,'\n');
fprintf(fid,k41);
fprintf(fid,'\n');
fprintf(fid,k51);
fprintf(fid,'\n');
fprintf(fid,k61);
fprintf(fid,'\n');
fprintf(fid,k71);
fprintf(fid,'\n');
fprintf(fid,k81);
fprintf(fid,'\n');
fprintf(fid,k91);
fprintf(fid,'\n');

fprintf(fid,'%X\n',I_size(1,:));

for ii = 1:size(I_GRAY,1)
    fprintf(fid,'%X\n',I_GRAY(ii,:));
end

fclose(fid);

# el archivo de salida Data_Image debe ser cargado en la memoria de datos


