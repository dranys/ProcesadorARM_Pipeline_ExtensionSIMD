 `timescale 1ns / 1ps  

module tb_demux_alu_in;
//Inputs
 logic[31:0] A,B;
 logic Vector;

//Outputs
 logic [31:0] S1, S2, S3, S4;

 demux_alu_in dem(
        .Vector(Vector),
        .SrcA(A),
        .SrcB(B),
        .S1(S1),
        .S2(S2),
        .S3(S3),
        .S4(S4)
        
     );

    initial begin
    // hold reset state for 100 ns.
      A = 32'heeeeeeee;
      B = 32'haaaaaaaa;
      Vector = 1'b0;
    end

    always begin

      #20;
      Vector = 1'b1;
      #20;
      Vector = 1'b0;

    end
endmodule