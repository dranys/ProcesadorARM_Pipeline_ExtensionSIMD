module Decode(
	input logic RegWriteW, clk, reset,
	input logic [3:0] WA3W, 
	input logic [31:0] InstrD, PCPlus8D, ResultW,
	output logic ALUSrcD, NoWriteD, BranchD, MemWriteD, MemtoRegD, RegWriteD, PCSrcD, Vector,
	output logic [1:0] ALUCtrD, FlagWriteD,
	output logic [3:0] CondD, WA3D, RA1D_out, RA2D_out, VALUctl,
	output logic [31:0] RD1, RD2, Extend
	//output logic [31:0] regOut[14:0]

);

parameter QUINCE = 4'd15;
logic [3:0] RA1D, RA2D;
logic [1:0] ImmSrc, RegSrc;


extend extend_(

.Instr(InstrD[23:0]),
.ImmSrc(ImmSrc),
.ExtImm(Extend)
);

controller CtlUnit(

.Op(InstrD[27:26]),
.Funct(InstrD[25:20]),
.Rd(InstrD[15:12]),
.Cond(InstrD[31:28]),
.FlagW(FlagWriteD),
.PCS(PCSrcD), 
.RegW(RegWriteD), 
.MemW(MemWriteD), 
.Branch(BranchD),
.MemtoReg(MemtoRegD), 
.ALUSrc(ALUSrcD),
.NoWrite(NoWriteD),
.ImmSrc(ImmSrc), 
.RegSrc(RegSrc), 
.ALUControl(ALUCtrD),
.VALUctl(VALUctl),
.Vector(Vector)

);

mux2 #(.WIDTH(4)) _1mux21
(
.d0(InstrD[19:16]), 
.d1(QUINCE),
.s(RegSrc[0]),
.y(RA1D)
);

mux2 #(.WIDTH(4)) _2mux21
(
.d0(InstrD[3:0]), 
.d1(InstrD[15:12]),
.s(RegSrc[1]),
.y(RA2D)
);

regfile RegisterFile(
.clk(clk),
.reset(reset),
.we3(RegWriteW),
.ra1(RA1D), 
.ra2(RA2D), 
.wa3(WA3W),
.wd3(ResultW), 
.r15(PCPlus8D),
.rd1(RD1), 
.rd2(RD2)
//.regOut(regOut)
);
assign CondD = InstrD[31:28];
assign WA3D = InstrD[15:12]; 

assign RA1D_out = RA1D;
assign RA2D_out = RA2D;
endmodule