// fpga4student.com: FPGA projects, Verilog projects, VHDL projects
// Verilog project: Verilog code for ALU
// by FPGA4STUDENT
 `timescale 1ns / 1ps  

module tb_alu;
//Inputs
 logic[7:0] A,B;
 logic[3:0] ALU_Sel;

//Outputs
 logic[7:0] ALU_Out;
 logic CarryOut;
 // Verilog code for ALU
 integer i;
 alu test_unit(
        .A(A),
        .B(B),  // ALU 8-bit Inputs                 
        .ALU_Sel(ALU_Sel),// ALU Selection
        .ALU_Out(ALU_Out), // ALU 8-bit Output
        .CarryOut(CarryOut) // Carry Out Flag
     );

    initial begin
    // hold reset state for 100 ns.
      A = 8'b11111111;
      B = 8'h02;

      ALU_Sel = 4'h0;
    end

    always begin

      #20;
      A = 8'b01;
      B = 8'h02;
      for (i=0;i<=15;i=i+1)
      begin
       ALU_Sel = ALU_Sel + 8'h01;
       #20;
      end;


    end
endmodule