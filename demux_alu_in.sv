module demux_alu_in_(
	input logic Vector,
	input logic [31:0] SrcA, SrcB,
	output logic [31:0] S1, S2, S3, S4
);

assign S1 = (Vector == 1'b0)? SrcA:32'b0;
assign S2 = (Vector == 1'b0)? SrcB:32'b0;
assign S3 = (Vector == 1'b1)? SrcA:32'b0;
assign S4 = (Vector == 1'b1)? SrcB:32'b0;

endmodule // demux_alu_in_