module dmem(

input logic clk, we,
input logic [31:0] a, wd, AddressToRead,
output logic [31:0] rd, ReadDirection, //out1, out2, out3, out4, out5, out6, out7, out8, out9, out10, out11, out12//, fin

output logic [31:0] RAM_O[100:0]


);
logic [31:0] RAM[100:0];

//
//logic [31:0] dir1 = 32'h14;//imagen
//logic [31:0] dir2 = 32'h18;
//logic [31:0] dir3 = 32'h1c;
//logic [31:0] dir4 = 32'h20;
//resultado encriptado
//logic [31:0] dir5 = 32'h2c;//44
//logic [31:0] dir6 = 32'h30;//48
//logic [31:0] dir7 = 32'h34;//52
//logic [31:0] dir8 = 32'h38;//56
//resultado desencriptado
//logic [31:0] dir9 = 32'h44;//68
//logic [31:0] dir10 = 32'h48;//72
//logic [31:0] dir11 = 32'h4c;//76
//logic [31:0] dir12 = 32'h50;//80

//initial begin

//$readmemh("Data_Image.di",RAM);
//end

assign rd = RAM[a[31:2]]; // word aligned
//assign RAM_O = RAM;

logic [12:0]dataR;

always_ff @(posedge clk)
begin
	if (we) RAM[a[31:2]] <= wd;
end	

always_ff @(negedge clk)
begin
	dataR = RAM[AddressToRead[31:2]];
end 

assign ReadDirection = dataR;
assign RAM_O = RAM[100:0];
/*assign out1 = RAM[dir1[31:2]];
assign out2 = RAM[dir2[31:2]];
assign out3 = RAM[dir3[31:2]];
assign out4 = RAM[dir4[31:2]];
assign out5 = RAM[dir5[31:2]];
assign out6 = RAM[dir6[31:2]];
assign out7 = RAM[dir7[31:2]];
assign out8 = RAM[dir8[31:2]];
*/
endmodule
