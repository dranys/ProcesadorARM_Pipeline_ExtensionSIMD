module demux_alu_out(
	input logic Vector,
	input logic [31:0] AluResultA, AluResultB,
	input logic [3:0] Vflags, flags,
	output logic [31:0] AluResult,
	output logic [3:0] AluFlags
);

assign AluResult = (Vector == 1'b0)? AluResultA:AluResultB;
assign AluFlags = (Vector == 1'b0)? flags:Vflags;

endmodule // demux_alu