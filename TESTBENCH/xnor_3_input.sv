module xnor_3_input(//A 00, B 01, C 10, D 11

	input logic A,B, C,
	output logic xnor_out
    );
	 
	 
 always_comb begin
 
	if(A == 0 && B == 0 && C == 1) xnor_out = 0;
	else if(A == 0 && B == 1 && C == 0) xnor_out = 0;
	else if(A == 1 && B == 0 && C == 0) xnor_out = 0;
	else xnor_out = 1;
	
 end
endmodule