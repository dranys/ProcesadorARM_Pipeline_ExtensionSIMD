`timescale  1ns/1ns
module ARM_Top_TB;

    //////////// CLOCK //////////
    logic CLOCK_50, CLOCK2_50, CLOCK3_50;

    //////////// LED //////////
    logic [8:0] LEDG;
    logic [17:0] LEDR;

    //////////// KEY //////////
    logic [3:0] KEY;

    //////////// SW //////////
    logic [17:0] SW;

    //////////// VGA //////////
    logic [7:0] VGA_B;
    logic VGA_BLANK_N, VGA_CLK;
    logic [7:0] VGA_G;
    logic VGA_HS;
    logic [7:0] VGA_R;
    logic VGA_SYNC_N, VGA_VS;

    logic [12:0] RAM[6000:0];
    logic [31:0] rf[14:0];
    logic [31:0] ALUResult_c;
//out put

logic [31:0] RamOut[100:0];
/*logic [31:0] data_out1;
logic [31:0] data_out2;
logic [31:0] data_out3;
logic [31:0] data_out4;
logic [31:0] data_out5;
logic [31:0] data_out6;
logic [31:0] data_out7;
logic [31:0] data_out8;
logic [31:0] data_out9;
logic [31:0] data_out10;
logic [31:0] data_out11;
logic [31:0] data_out12;*/

ARM_Pipeline_processor_top dut
(
.CLOCK_50(CLOCK_50),
.CLOCK2_50(CLOCK2_50),
.CLOCK3_50(CLOCK3_50),
.LEDG(LEDG),
.LEDR(LEDR),
.KEY(KEY),
.SW(SW),
.VGA_B(VGA_B),
.VGA_BLANK_N(VGA_BLANK_N),
.VGA_CLK(VGA_CLK),
.VGA_G(VGA_G),
.VGA_HS(VGA_HS),
.VGA_R(VGA_R),
.VGA_SYNC_N(VGA_SYNC_N),
.VGA_VS(VGA_VS),
.ALUResult_c(ALUResult_c),
.RamOut(RamOut)
/*.memory_out1(data_out1),
.memory_out2(data_out2),
.memory_out3(data_out3),
.memory_out4(data_out4),
.memory_out5(data_out5),
.memory_out6(data_out6),
.memory_out7(data_out7),
.memory_out8(data_out8),
.memory_out9(data_out9),
.memory_out10(data_out10),
.memory_out11(data_out11),
.memory_out12(data_out12)*/
);

	
	initial begin
		
		KEY[0] = 1'b1;
		#5;
		KEY[0] = 1'b0;
        #20;
        SW[0] = 1;
        #20;
        SW[0] = 0;
		#100;
	end // initial

always 
	begin
		CLOCK_50 <= 1; #5;
		CLOCK_50 <= 0; #5;

	end

endmodule
