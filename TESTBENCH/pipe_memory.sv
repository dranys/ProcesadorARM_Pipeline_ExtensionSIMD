`timescale 1ns / 1ps

module pipe_memory(clk, in_1_E, in_2_E, in_3_E, in_4_E, ALUResult_E, WriteDataE, WA3E, 
PCSrcM, RegWriteM, MemtoRegM, MemWriteM, ALUResult_M, WriteDataM, WA3M);
	
	
	input logic clk;
	input logic in_1_E;
	input logic in_2_E;
	input logic in_3_E;
	input logic in_4_E;
	input logic [31:0] ALUResult_E;
	input logic [31:0] WriteDataE;
	input logic [3:0] WA3E;

	
	output logic PCSrcM;
	output logic RegWriteM;
	output logic MemtoRegM;
	output logic MemWriteM;
	output logic [31:0] ALUResult_M;
	output logic [31:0] WriteDataM;
	output logic [3:0] WA3M;
	
	

	always_ff @(negedge clk)begin
		
			PCSrcM = in_1_E;
			RegWriteM = in_2_E;
			MemtoRegM = in_3_E;
			MemWriteM = in_4_E;
			ALUResult_M = ALUResult_E;
			WriteDataM = WriteDataE;
			WA3M = WA3E;
	
	end	

endmodule