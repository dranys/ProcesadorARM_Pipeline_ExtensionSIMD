`timescale 1ns / 1ps

module pipe_decode(clk,Enable, InstF, InstD);
	input logic clk, Enable;
	input logic [31:0] InstF;
	output logic[31:0] InstD;
	

	always_ff @(negedge clk)begin
	
		if (~Enable)begin
			InstD = InstF;
		end 
	end	

endmodule