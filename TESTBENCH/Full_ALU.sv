module Full_ALU(
	input logic [1:0] ALUControl,
	input logic [31:0] A, B,
	output logic [31:0] RESULTADO,
	output logic [3:0] ALUFlags
);

logic Cout;
logic Zero;
logic Negative;
logic Carry;
logic Overflow;
logic [31:0] Suma;
logic p_overflow1;

ALU alu(
.A(A),
.B(B),
.RESULTADO(RESULTADO),
.Cout(Cout),
.Suma(Suma),
.ALUctl(ALUControl)
);

xnor_3_input xnor_(
.A(ALUControl[0]),
.B(A[31]),
.C(B[31]),
.xnor_out(p_overflow1)
);

always_comb begin
	Overflow = ~ALUControl[1] & (A[31] ^ Suma[31]) & p_overflow1;
	Carry =  (~ALUControl[1]) & Cout;
	Negative = RESULTADO[31];
	Zero = (RESULTADO == 32'b0)? 1:0;
end
assign ALUFlags = {Negative,Zero,Carry,Overflow};
endmodule


