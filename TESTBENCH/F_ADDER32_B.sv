module F_ADDER32_B(
	input logic [31:0] A,B,
	input logic Cin,
	output logic [31:0] S,
	output logic Cout
    );
	 logic [32:0] Sum;//aqui se guarda toda la suma
	 assign Sum = A + B + Cin;//obtendo la suma total
	 assign Cout = Sum[32];// y a partir de Sum mi carry de salida
	 assign S = A + B + Cin; // la suma toma nada mas los primeros 32 bit de sum que tiene 33 bit
	 
endmodule