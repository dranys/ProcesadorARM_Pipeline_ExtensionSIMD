module image_printer (
    // INPUTS
    input logic [31:0] pixel_value,
    input logic oRequest,
    input logic oVGA_H_SYNC,
    input logic oVGA_V_SYNC,
	  input logic img_selector,
    input logic enable,
    input logic clk,
    // OUTPUTS
    output logic [31:0] pix_address,
    output logic [9:0] r_out,
    output logic [9:0] g_out,
    output logic [9:0] b_out,
    output logic vga_reset_n
);
// dirección initial para la imagen
parameter img_init_address = 44;

// direcciones para las dimensiones de la imagen
parameter img_dimension_width_addr = 40;
parameter img_dimension_height_addr = 36;

//registros para guardar direcciones de imgen y valores de la imagen
logic [31:0] img_f_init_address;

logic [31:0] img_dimension_height;

logic [31:0] img_dimension_width;

logic [31:0] img_f_dimension_height;

logic [31:0] img_f_dimension_width;

// bandera para los pasos del setup
logic [1:0] ready_state;

logic setup_flag;

// contadores de img
logic [31:0] img_v_counter;

logic [31:0] img_h_counter;

// contadores de la pantalla
logic [31:0] vga_v_counter;

logic [31:0] vga_h_counter;

// limites para la imagen a la hora de pintarla
logic [31:0] img_screen_height;

logic [31:0] img_screen_width;

logic [31:0] img_screen_init_address;

// direccion actual
logic [31:0] act_pix_address;

always @ (posedge clk ) begin

  if (!enable) begin // valores iniciales
    ready_state = 0;
    setup_flag = 0;
    r_out <= 0;
    g_out <= 0;
    b_out <= 0;
    img_h_counter <= 0;
    img_v_counter <= 0;
	  vga_h_counter <= 1;
	  vga_v_counter <= 1;
    pix_address <= img_dimension_height_addr;
    vga_reset_n = 0;
  end else begin // se empieza a hacer fetch de la informacion
    if (!setup_flag) begin
      case (ready_state)
        2'b00: begin
          img_dimension_height= pixel_value;
          pix_address = img_dimension_width_addr;
          ready_state = 2'b01;
        end
        2'b01: begin
          img_dimension_width  = pixel_value;
          ready_state = 2'b10;
        end
        2'b10: begin
          img_f_dimension_width= img_dimension_height - 2;
           img_f_dimension_height = img_dimension_width - 2;
          img_f_init_address = 8'h4*(img_dimension_width*img_dimension_height) + img_init_address;
          if (!img_selector) begin
            img_screen_init_address = img_init_address;
            pix_address = img_init_address;
            img_screen_width = img_dimension_width;
            img_screen_height = img_dimension_height;
          end else begin
            img_screen_init_address = img_f_init_address;
            pix_address = img_f_init_address;
            img_screen_width = img_f_dimension_width;
            img_screen_height = img_f_dimension_height;
          end
          setup_flag = 1;
        end
        default: ready_state = 2'b00;
      endcase
    end else begin
      vga_reset_n = 1;
      if (img_v_counter < img_screen_height) begin
        if (!oRequest && img_h_counter == 0 ) begin
          // muestra pixel
          r_out = 0;
          g_out = 0;
          b_out = 0;
        end else if (oRequest && img_h_counter < img_screen_width && oVGA_H_SYNC) begin
			if(pixel_value > 8000)	begin
				 r_out = 1;
				 g_out = 1;
				 b_out = 1;
			end else if(pixel_value> 255 && pixel_value < 8000)begin 
				 r_out = 254;
				 g_out = 254;
				 b_out = 254;
			end else begin
				// muestra pixel
				 r_out = pixel_value[9:0];
				 g_out = pixel_value[9:0];
				 b_out = pixel_value[9:0];
			end
          // aumenta contadores
          img_h_counter = img_h_counter + 8'h1;
          // aumenta direccion
          pix_address = pix_address + 8'h4;
        end else if (img_h_counter >= img_screen_width && oVGA_H_SYNC) begin
          // muestra pixel
          r_out = 0;
          g_out = 0;
          b_out = 0;
        end  else if (!oVGA_H_SYNC) begin
          // muestra pixel
          r_out = 0;
          g_out = 0;
          b_out = 0;
          // resetea contadores
          img_h_counter = 0;
          img_v_counter = img_v_counter + 1;
        end
      end else if (oVGA_V_SYNC) begin
        r_out = 0;
        g_out = 0;
        b_out = 0;
      end else begin
        //reseto
        r_out = 0;
        g_out = 0;
        b_out = 0;

        img_v_counter = 0;
        img_h_counter = 0;

        pix_address = img_screen_init_address;
      end
    end
  end
end

endmodule // image_printer
