module ALU(
output logic[31:0] RESULTADO, Suma,
output logic Cout,
input logic [31:0] A, B,
input logic [1:0] ALUctl);

logic [31:0] P1;
logic [31:0] P2;

MUX2_1_32bits mux21(
.A(B),
.B(~B),
.sel(ALUctl[0]),
.Mout(P1)

);

F_ADDER32_B F_adder(
.A(A),
.B(P1),
.Cin(ALUctl[0]),
.Cout(Cout),
.S(P2)
);


MUX4_1_32bits mux41(
.A(P2),//suma
.B(P2),//resta
.C(A^B),//xor
.D(A*B),//multiplication
.sel(ALUctl),
.Mout(RESULTADO)
);
assign Suma = P2;

endmodule 
