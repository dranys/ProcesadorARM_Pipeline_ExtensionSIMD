module Fetch(

input logic PCSrcW, clk, reset, StallF,
input logic [31:0] ResultW,
output logic [31:0] InstrF,
output logic [31:0] PCPlus4

);

logic [31:0] PC, PCF, PCPplus_4;

parameter CUATRO=32'd4;

adder #(.WIDTH(32)) adder
(
.a(PCF), 
.b(CUATRO),
.y(PCPplus_4)
);

mux2 #(.WIDTH(32)) mux21
(
.d0(PCPplus_4),
.d1(ResultW),
.s(PCSrcW),
.y(PC)
);

flopenr #(.WIDTH(32)) flipflop(

.clk(clk), 
.en(StallF),
.reset(reset), 
.d(PC),
.q(PCF)

);


imem InstructionMemory(

.a(PCF),
.rd(InstrF)
);


assign PCPlus4 = PCPplus_4;



endmodule