// fpga4student.com: FPGA projects, Verilog projects, VHDL projects
// Verilog project: Verilog code for ALU
// by FPGA4STUDENT
 `timescale 1ns / 1ps  

module tb_vec_alu;
//Inputs
 logic[31:0] A,B;
 logic[3:0] ALU_Sel;

//Outputs
 logic[31:0] ALU_Out;
 logic[3:0] CarryOut;

 // Verilog code for ALU
 integer i;

 VecALU test_VecAlu(
        .VecA(A),
        .VecB(B),  // ALU 8-bit Inputs                 
        .Selector(ALU_Sel),// ALU Selection
        .VecCarryFlags(CarryOut),
        .VecAluResult(ALU_Out) // Carry Out Flag
     );

    initial begin
    // hold reset state for 100 ns.
      A = 32'habcabcab;
      B = 32'hac;

      ALU_Sel = 4'h0;
    end

    always begin

      #20;
      for (i=0;i<=15;i=i+1)
      begin
       ALU_Sel = ALU_Sel + 4'h01;
       #20;
      end;


    end
endmodule