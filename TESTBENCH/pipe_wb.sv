`timescale 1ns / 1ps

module pipe_write_back(clk, PCSrcM, RegWriteM, MemtoRegM, RD_in, ALUResult_M, WA3M,
PCSrcWB, RegWriteWB, MemtoRegWB, RD_out, ALUResult_WB, WA3W);
	
	
	input logic clk;
	input logic PCSrcM;
	input logic RegWriteM;
	input logic MemtoRegM;
	input logic [31:0] RD_in;
	input logic [31:0] ALUResult_M;
	input logic [3:0] WA3M;

	
	output logic PCSrcWB;
	output logic RegWriteWB;
	output logic MemtoRegWB;
	output logic [31:0] RD_out;
	output logic [31:0] ALUResult_WB;
	output logic [3:0] WA3W;
	
	

	always_ff @(negedge clk)begin
		
			PCSrcWB = PCSrcM;
			RegWriteWB = RegWriteM;
			MemtoRegWB = MemtoRegM;
			RD_out = RD_in;
			ALUResult_WB = ALUResult_M;
			WA3W = WA3M;
	
	end	

endmodule