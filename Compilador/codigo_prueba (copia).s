		ADD		R1,R1,	#36; se carga 36 para apuntar la direccion
		LDR		R2,[R1] ; carga j filas
		ADD		R1,R1,	#4 ; se carga 40 para apuntar la direccion
		LDR		R3,[R1] ; carga i columnas
		EOR		R10,R10,R10 ;contador i columnas
		EOR		R11,R11,R11 ;contador j filas
		ADD		R1,R1,	#4 ; se carga 44 para apuntar la direccion
		ADD		R7,R1,	#0 ;carga la direccion del  primer pixel de la imagen ****deberia de ser 44	
		MUL 	R13,R2,	#4 ; calculo 1: direccion para resultado del primer pixel	
		MUL     R13,R3,R13 ; calculo 2: direccion para resultado del primer pixel
		ADD		R13,R13,R7 ; calculo 3: direccion para resultado del primer pixel
		EOR		R1,R1,R1 ; para que apunte a 0
		MUL 	R12,R3,	#4 ;calcula desplazamiento
		SUB		R2,R2,	#2 ; se resta i-2
		SUB		R3,R3,	#2 ; se resta j-2
		ADD		R8,R7,	#4	;R8 tiene la direccion de R7 + 4
		ADD		R9,R7,	#8 ;R9 tiene la direccion de R8 + 4
CONV:	EOR 	R6,R6,R6	; se reinicia el acumulador
		LDR		R0,[R1] ; carga k 1,1
		LDR		R4,[R7] ;carga primer pixel ****************
		MUL		R6,R4,R0 ;multiplicar elemento del kernel con el pixel
		ADD		R1,R1,	#4 ;carga la direccion de k1,2
		LDR		R0,[R1] ; carga k 1,2
		LDR		R4,[R8] ; carga segundo pixel
		MUL		R5,R4,R0 ;multiplicar elemento del kernel con el pixel
		ADD		R6,R6,R5 ;suma el valor acumulado
		ADD		R1,R1,	#4 ;carga la direccion de k1,3
		LDR		R0,[R1] ; carga k 1,3
		LDR		R4,[R9] ; carga tercer pixel
		MUL		R5,R4,R0 ;multiplicar elemento del kernel con el pixel
		ADD		R6,R6,R5 ;suma el valor acumulado
		ADD		R7,R7,R12 ; nueva direcion para el pixel 2,1
		ADD		R8,R8,R12 ; nueva direcion para el pixel 2,2
		ADD		R9,R9,R12 ; nueva direcion para el pixel 2,3
		ADD		R1,R1,#4 ;carga la direccion de k2,1
		LDR		R0,[R1] ; carga k 2,1
		LDR		R4,[R7] ;carga  pixel 2,1
		MUL		R5,R4,R0 ;multiplicar elemento del kernel con el pixel
		ADD		R6,R6,R5 ;suma el valor acumulado
		ADD		R1,R1,	#4 ;carga la direccion de k2,2
		LDR		R0,[R1] ; carga k 2,2
		LDR		R4,[R8] ;carga  pixel 2,2
		MUL		R5,R4,R0 ;multiplicar elemento del kernel con el pixel
		ADD		R6,R6,R5 ;suma el valor acumulado

		ADD		R1,R1,	#4 ;carga la direccion de k2,3
		LDR		R0,[R1] ; carga k 2,3
		LDR		R4,[R9] ;carga  pixel 2,3
		MUL		R5,R4,R0 ;multiplicar elemento del kernel con el pixel
		ADD		R6,R6,R5 ;suma el valor acumulado
		ADD		R7,R7,R12 ; nueva direcion para el pixel 3,1
		ADD		R8,R8,R12 ; nueva direcion para el pixel 3,2
		ADD		R9,R9,R12 ; nueva direcion para el pixel 3,3
		ADD		R1,R1,	#4 ;carga la direccion de k3,1
		LDR		R0,[R1] ; carga k 3,1
		LDR		R4,[R7] ;carga  pixel 3,1
		MUL		R5,R4,R0 ;multiplicar elemento del kernel con el pixel
		ADD		R6,R6,R5 ;suma el valor acumulado
		ADD		R1,R1,	#4 ;carga la direccion de k3,2
		LDR		R0,[R1] ; carga k 3,2
		LDR		R4,[R8] ;carga  pixel 3,2
		MUL		R5,R4,R0 ;multiplicar elemento del kernel con el pixel
		ADD		R6,R6,R5 ;suma el valor acumulado

		ADD		R1,R1,	#4 ;carga la direccion de k3,3
		LDR		R0,[R1] ; carga k 3,3
		LDR		R4,[R9] ;carga  pixel 3,3
		MUL		R5,R4,R0 ;multiplicar elemento del kernel con el pixel
		ADD		R6,R6,R5 ;suma el valor acumulado
	
		CMP 	R11,#0
		BEQ		NEXT
		ADD		R6,R6,#3

NEXT:	STR		R6,[R13]	;se alamacena el resultado de la convolcuon del kernel y las celdas de pixeles
		ADD R10,R10,	#1 ; se suma una unidad al contador i
		CMP	R10,R2 ;se comparan los i para ver como se reinician las direcciones
		BNE	DESX	; se desplazan los indices a la derecha
		;nop
		;nop
		;nop
		;nop
		ADD R11,R11,	#1 ; se suma una unidad al contador j
		CMP R11,R3 ;se comparan los j para ver si se termina la ejecucion
		BEQ	EXIT
		;nop
		;nop
		;nop
		;nop
		B   DESY	; se reinician los indices hacia abajo
		;nop
		;nop
		;nop
		;nop
DESX:	SUB	R7,R7,R12
		SUB	R8,R8,R12
		SUB	R9,R9,R12
		SUB	R7,R7,R12
		SUB	R8,R8,R12
		SUB	R9,R9,R12
		ADD R7,R7,	#4
		ADD R8,R8,	#4
		ADD R9,R9,	#4
		EOR R1,R1,R1	; se reinicia el puntero del kernel
		ADD R13,R13,	#4 ; nueva direccion para almacenar
		B CONV
		;nop
		;nop
		;nop
		;nop
DESY:	MUL 	R7,R12,R11  ; se busca la direccion fila a cargar
		ADD		R7,R7,	#44 ; se suma el valor inicial del primer pixel de la imagen 		
		ADD		R8,R7,	#4	;R8 tiene la direccion de R7 + 4
		ADD		R9,R7,	#4 ;R9 tiene la direccion de R8 + 4 								
		EOR R10,R10,R10 ; se reinicia el contador i
		EOR R1,R1,R1	; se reinicia el puntero del kernel
		ADD R13,R13,	#4 ; nueva direccion para almacenar
		B CONV
		;nop
		;nop
		;nop
		;nop
EXIT:	B EXIT
		;nop
		;nop
		;nop
		;nop

