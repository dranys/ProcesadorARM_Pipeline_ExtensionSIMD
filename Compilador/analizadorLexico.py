import ply.lex as lex
import re
import codecs
import os
import sys

clase = 0

errL = False
isComment = 0

# Tokens para catalogar los lexemas
tokens = ['ID','RPC','LPC','NUMBER','REG','COMMA','MINUS','NUM','DP']


kword = ['ADD','SUB','MUL','B','BEQ','BNE','CMP','STR','LDR','EOR','NOP','VADD','VSUB','VXOR','VSR','VSL','VSCR','VSCL','VNOR','VOR','VNAND','VAND','VMUL']
tokens = tokens+ kword

t_RPC = r'\]'
t_LPC = r'\['
t_COMMA = r','
t_MINUS = r'\-'
t_NUM = r'\#'
t_DP = r'\:'

def t_ADD(t):
    r'ADD|add'
    t.value = t.value.upper()
    return t

def t_SUB(t):
    r'SUB|sub'
    t.value = t.value.upper()
    return t

def t_MUL(t):
    r'MUL|mul'
    t.value = t.value.upper()
    return t


def t_CMP(t):
    r'CMP|cmp'
    t.value = t.value.upper()
    return t

def t_NOP(t):
    r'NOP|nop'
    t.value = t.value.upper()
    return t

def t_STR(t):
    r'STR|str'
    t.value = t.value.upper()
    return t

def t_LDR(t):
    r'LDR|ldr'
    t.value = t.value.upper()
    return t

def t_BEQ(t):
    r'BEQ|beq'
    t.value = t.value.upper()
    return t

def t_BNE(t):
    r'BNE|bne'
    t.value = t.value.upper()
    return t

def t_EOR(t):
    r'EOR|eor'
    t.value = t.value.upper()
    return t

def t_B(t):
    r'B|b'
    t.value = t.value.upper()
    return t

def t_VADD(t):
    r'VADD|vadd'
    t.value = t.value.upper()
    return t

def t_VSUB(t):
    r'VSUB|vsub'
    t.value = t.value.upper()
    return t

def t_VXOR(t):
    r'VXOR|vxor'
    t.value = t.value.upper()
    return t

def t_VSR(t):
    r'VSR|vsr'
    t.value = t.value.upper()
    return t

def t_VSL(t):
    r'VSL|vsl'
    t.value = t.value.upper()
    return t

def t_VMUL(t):
    r'VMUL|vmul'
    t.value = t.value.upper()
    return t

def t_VSCR(t):
    r'VSCR|vscr'
    t.value = t.value.upper()
    return t

def t_VSCL(t):
    r'VSCL|vscl'
    t.value = t.value.upper()
    return t

def t_VOR(t):
    r'VOR|vor'
    t.value = t.value.upper()
    return t

def t_VNOR(t):
    r'VNOR|vnor'
    t.value = t.value.upper()
    return t

def t_VAND(t):
    r'VAND|vand'
    t.value = t.value.upper()
    return t

def t_VNAND(t):
    r'VNAND|vnand'
    t.value = t.value.upper()
    return t

def t_ID(t):
    r'[a-qs-zA-QS-Z][a-zA-Z0-9]*'
    return t

def t_REG(t):
    r'[rR][0-9]+'
    t.value = t.value.upper()
    return t

def t_COMMENT(t):
    r';.*'
    global isComment
    isComment += 1
    t.lexer.skip(1)

def t_NUMBER(t):
    r'\d+|\-\d+'
    return t

def t_error(t):
    global errL
    errL = True
    print("Aqui hay un error",t.value[0])
    t.lexer.skip(1)
    
def t_nonespace(t):
    r'\s+'
    pass

def getErrL():
    global errL
    return errL










        
