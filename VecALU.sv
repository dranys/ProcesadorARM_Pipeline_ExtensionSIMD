module VecALU(
	input logic [31:0] VecA, VecB,
	input logic [3:0] Selector,
	output logic [3:0] VecCarryFlags,
	output logic [31:0] VecAluResult
);

logic [7:0] resultAlu1;
logic [7:0] resultAlu2;
logic [7:0] resultAlu3;
logic [7:0] resultAlu4;

logic Carry1;
logic Carry2;
logic Carry3;
logic Carry4;


 alu Alu1(
        .A(VecA[7:0]),
        .B(VecB[7:0]),  // ALU 8-bit Inputs                 
        .ALU_Sel(Selector),// ALU Selection
        .ALU_Out(resultAlu1), // ALU 8-bit Output
        .CarryOut(Carry1) // Carry Out Flag
     );

  alu Alu2(
        .A(VecA[15:8]),
        .B(VecB[15:8]),  // ALU 8-bit Inputs                 
        .ALU_Sel(Selector),// ALU Selection
        .ALU_Out(resultAlu2), // ALU 8-bit Output
        .CarryOut(Carry2) // Carry Out Flag
     );


 alu Alu3(
        .A(VecA[23:16]),
        .B(VecB[23:16]),  // ALU 8-bit Inputs                 
        .ALU_Sel(Selector),// ALU Selection
        .ALU_Out(resultAlu3), // ALU 8-bit Output
        .CarryOut(Carry3) // Carry Out Flag
     );


 alu Alu4(
        .A(VecA[31:24]),
        .B(VecB[31:24]),  // ALU 8-bit Inputs                 
        .ALU_Sel(Selector),// ALU Selection
        .ALU_Out(resultAlu4), // ALU 8-bit Output
        .CarryOut(Carry4) // Carry Out Flag
     );

assign VecCarryFlags = {Carry4,Carry3,Carry2,Carry1};
assign VecAluResult = {resultAlu4,resultAlu3,resultAlu2,resultAlu1};
endmodule // VecALU


