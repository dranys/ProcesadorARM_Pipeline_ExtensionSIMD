module Execute(
input logic PCSrcE,NoWrite, RegWriteE, MemWriteE, BranchE, ALUSrcE, clk, reset, VectorE,
input logic [1:0] FlagsWriteE, ForwardAE, ForwardBE, ALUControlE,
input logic [3:0] CondE, FlagsE, VALUctlE,
input logic [31:0] RD1E, RD2E, ExtendE, ResultW, ALUResultM,
output logic [3:0] ALUFlagsFeedback,
output logic PCSrc_E, RegWrite_E, MemWrite_E,
output logic [31:0] ALUResultE, WriteDataE
);

logic [31:0] SrcAE, SrcBE, mux31out; 
logic [3:0] ALUFlags;
logic PCSrc,BrancheEout;

//conexion para la alu simple
logic [31:0] ASA,ASB,AR;//alu sourceA,sourceB, alu result
logic [3:0] Aflgs;

//conexion para la alu vectorial
logic [31:0] VASA,VASB,VAR;//vector alu sourceA,sourceB,vector alu result
logic [3:0] VAflgs;


mux3 #(.WIDTH(32)) mux31_1
(
.d0(RD1E),
.d1(ResultW), 
.d2(ALUResultM),
.s(ForwardAE),
.y(SrcAE));

mux3 #(.WIDTH (32)) mux31_2
(
.d0(RD2E),
.d1(ResultW), 
.d2(ALUResultM),
.s(ForwardBE),
.y(mux31out)

);

mux2 #(.WIDTH (32)) mux21
(
.d0(mux31out), 
.d1(ExtendE),
.s(ALUSrcE),
.y(SrcBE)
);

Full_ALU ALU(
.ALUControl(ALUControlE),
.A(ASA), 
.B(ASB),
.RESULTADO(AR),
.ALUFlags(Aflgs)
);

demux_alu_in_ demIn(
	.Vector(VectorE),
	.SrcA(SrcAE), 
	.SrcB(SrcBE),
	.S1(ASA), 
	.S2(ASB), 
	.S3(VASA), 
	.S4(VASB)
);

VecALU Aluvectorial(
	.VecA(VASA), 
	.VecB(VASB),
	.Selector(VALUctlE),
	.VecCarryFlags(VAflgs),
	.VecAluResult(VAR)
);

demux_alu_out demOut(
	.Vector(VectorE),
	.AluResultA(AR), 
	.AluResultB(VAR),
	.Vflags(VAflgs), 
	.flags(Aflgs),
	.AluResult(ALUResultE),
	.AluFlags(ALUFlags)
);


condlogic CondLogic(

.clk(clk), 
.reset(reset),
.BranchE(BranchE),
.NoWrite(NoWrite),
.Cond(CondE),
.ALUFlags(ALUFlags),
.ALUFlags_feet(FlagsE),
.FlagW(FlagsWriteE),
.ALUFlags_feet_out(ALUFlagsFeedback),
.PCS(PCSrcE), 
.RegW(RegWriteE), 
.MemW(MemWriteE),
.PCSrc(PCSrc), 
.RegWrite(RegWrite_E), 
.BrancheEout(BrancheEout), 
.MemWrite(MemWrite_E)
);

assign PCSrc_E = BrancheEout | PCSrc;

assign WriteDataE = mux31out;


endmodule