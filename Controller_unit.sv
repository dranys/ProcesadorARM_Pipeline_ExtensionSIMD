module controller(

input logic [1:0] Op,
input logic [5:0] Funct,
input logic [3:0] Rd, Cond,//-V.cond
output logic [1:0] FlagW,
output logic PCS, RegW, MemW, Branch, Vector, //- Vector
output logic MemtoReg, ALUSrc, NoWrite,
output logic [1:0] ImmSrc, RegSrc, ALUControl,
output logic [3:0] VALUctl);// vector alu control

logic [9:0] controls;
logic ALUOp;



// Main Decoder

always_comb

casex(Op)
// Data-processing immediate
2'b00: if (Funct[5]) controls = 10'b0000101001;
// Data-processing register
else controls = 10'b0000001001;
// LDR
2'b01: if (Funct[0]) controls = 10'b0001111000;
// STR
else controls = 10'b1001110100;
// B
2'b10: controls = 10'b0110100010;
// Unimplemented
default: controls = 10'bx;
endcase
assign {RegSrc, ImmSrc, ALUSrc, MemtoReg,
RegW, MemW, Branch, ALUOp} = controls;


// ALU Decoder
always_comb 
	if (ALUOp) begin // which DP Instr?
		case(Funct[4:1])
			4'b0000: begin
				ALUControl = 2'b10; // AND
				VALUctl = 4'b0;//VADD
			end
			4'b0001: begin
				ALUControl = 2'bx;
				VALUctl = 4'b0001;//vsub
			end
			4'b0010: begin
				ALUControl = 2'b01; // SUB
				VALUctl = 4'b0010;//VMUL
				
			end
			4'b0011: begin
				ALUControl = 2'bx;
				VALUctl = 4'b0011;//vxor
			end
			4'b0100: begin
				ALUControl = 2'b00; // ADD
				VALUctl = 4'b0100;// Logical shift left
			end
			4'b0101: begin
				ALUControl = 2'bx;
				VALUctl = 4'b0101;//Logical shift right
			end
			4'b0110: begin
				ALUControl = 2'bx;
				VALUctl = 4'b0110;//// Rotate left
			end
			4'b0111: begin
				ALUControl = 2'bx;
				VALUctl = 4'b0111;//// Rotate right
			end
			4'b1000: begin
				ALUControl = 2'bx;
				VALUctl = 4'b1000;//VAND
			end
			4'b1001: begin
				ALUControl = 2'bx;
				VALUctl = 4'b1001;//VOR
			end
			4'b1010: begin
				ALUControl = 2'b01;//CMP--
				VALUctl = 4'b1010;//UNIMPLEMENTED
			end
			4'b1011: begin
				ALUControl = 2'bx;
				VALUctl = 4'b1011;//VNOR
			end
			4'b1100: begin
				ALUControl = 2'b11; // ORR
				VALUctl = 4'b1100;// Logical nand 
			end
			4'b1101: begin
				ALUControl = 2'bx;
				VALUctl = 4'b1101;// Logical xnor
			end
			4'b1110: begin
				ALUControl = 2'bx;
				VALUctl = 4'bx;// unimplemented 
			end
			4'b1111: begin
				ALUControl = 2'bx;
				VALUctl = 4'bx;// unimplemented  
			end
			default: begin
				ALUControl = 2'bx; // unimplemented
				VALUctl = 4'bx;//
			end
		endcase
// update flags if S bit is set (C & V only for arith)
		
		FlagW[1] = Funct[0];
		FlagW[0] = Funct[0] &
		(ALUControl == 2'b00 | ALUControl == 2'b01);
		NoWrite = (Funct[4:1] == 4'b1010) ? 1'b1:1'b0;//CMP--
	end 

	else begin
		VALUctl = 4'b0;//
		ALUControl = 2'b00; // add for non-DP instructions
		FlagW = 2'b00; // don't update Flags
		NoWrite = 1'b0;//CMP--
	end
// PC Logic
	assign PCS = ((Rd == 4'b1111) & RegW) | Branch;
	assign Vector = (Cond == 4'b1111)? 1'b1:1'b0;//establece la bandera para utilizar la alu vectorial
endmodule // controller
