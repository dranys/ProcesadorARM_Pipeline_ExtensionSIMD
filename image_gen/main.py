import numpy as np
import cv2
from bitstring import BitArray

image = cv2.imread("/home/brais/Workspace/Arqui2/Proyecto2/PruebasFPGA/ImageReader/image_gen/ip_lena.png")
image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
newGray = cv2.resize(image_gray, (200, 200))

cv2.imwrite('newGrayPicture.jpg', newGray)

#header
f = open("RAM.mif", "w+")

f.write("WIDTH = 32;")
f.write("\n")
f.write("DEPTH = 32768;")
f.write("\n")
f.write("\n")

f.write("ADDRESS_RADIX = UNS;")
f.write("\n")
f.write("DATA_RADIX = BIN;")
f.write("\n")
f.write("\n")
f.write("CONTENT BEGIN")
f.write("\n")

#header

row, colum, counter = 0, 0, 0

height, width = newGray.shape[:2]

f.write("\t")
f.write(str(1) + " : " + str(0).zfill(32) + ";")
f.write("\n")
f.write("\t")
f.write(str(2) + " : " + str(0).zfill(32) + ";")
f.write("\n")
f.write("\t")
f.write(str(3) + " : " + str(0).zfill(32) + ";")
f.write("\n")
f.write("\t")
f.write(str(4) + " : " + str(0).zfill(32) + ";")
f.write("\n")
f.write("\t")
f.write(str(5) + " : " + str(1000).zfill(32) + ";")
f.write("\n")
f.write("\t")
f.write(str(6) + " : " + (str(bin(10009))[2:]).zfill(32) + ";")
f.write("\n")
f.write("\t")
f.write(str(7) + " : " + (str(bin(20009))[2:]).zfill(32) + ";")
f.write("\n")


while(row < height):
	while(colum < width):

		pixel1 = newGray[row, colum]
		pixel2 = newGray[row, colum + 1]
		pixel3 = newGray[row, colum + 2]
		pixel4 = newGray[row, colum + 3]

		pixel1 = str(bin(pixel1)[2:].zfill(8))
		pixel2 = str(bin(pixel2)[2:].zfill(8))
		pixel3 = str(bin(pixel3)[2:].zfill(8))
		pixel4 = str(bin(pixel4)[2:].zfill(8))

		pixels = pixel1 + pixel2 + pixel3 + pixel4
		f.write("\t")
		f.write(str(counter+8) + " : " + pixels + ";")
		f.write("\n")
		colum += 4
		counter += 1
	row += 1
	colum = 0

f.write("\t")
f.write(str(counter+8) + " : " + str(1).zfill(32) + ";")
f.write("\n")

f.write("\t")
f.write("[" + str(counter+8+1) + ".." + str(32767) + "]" + " : " + str(bin(0)[2:].zfill(32)) + ";")
f.write("\n")
f.write("END;")
f.close()
