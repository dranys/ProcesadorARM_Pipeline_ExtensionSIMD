`timescale 1ns / 1ps

module pipe_execute(clk,Clear, NoWriteD, NoWriteE, Cond_3128_D, VectorD, VALUctlD, VALUctlE, VectorE, Cond_3128_E, Cond_1512_E, Cond_1512_D, PCSrcD,RegWriteD, MemtoRegD, MemWriteD, ALUControlD, BranchD,
ALUSrcD, FlagWriteD, Flags_in, Flags_out, RD1_D, RD2_D, ExtImmD, RA1D, RA2D, PCSrcE,RegWriteE, 
MemtoRegE, MemWriteE, ALUControlE, BranchE, ALUSrcE, FlagWriteE, RD1_E, RD2_E, ExtImmE, RA1E, RA2E);
	
	
	input logic clk, Clear, NoWriteD,VectorD;
	input logic [3:0] Cond_3128_D,VALUctlD;
	input logic [3:0] Cond_1512_D;
	input logic PCSrcD;
	input logic RegWriteD;
	input logic MemtoRegD;
	input logic MemWriteD;
	input logic [1:0] ALUControlD;
	input logic BranchD;
	input logic ALUSrcD;
	input logic [1:0] FlagWriteD;
	input logic [3:0] Flags_in;
	input logic [31:0] RD1_D;
	input logic [31:0] RD2_D;
	input logic [31:0] ExtImmD;
	input logic [3:0] RA1D;
	input logic [3:0] RA2D;
	
	output logic [3:0] Cond_3128_E, VALUctlE;
	output logic [3:0] Cond_1512_E;
	output logic PCSrcE, NoWriteE,VectorE;
	output logic RegWriteE;
	output logic MemtoRegE;
	output logic MemWriteE;
	output logic [1:0] ALUControlE;
	output logic BranchE;
	output logic ALUSrcE;
	output logic [1:0] FlagWriteE;
	output logic [3:0] Flags_out;
	output logic [31:0] RD1_E;
	output logic [31:0] RD2_E;
	output logic [31:0] ExtImmE;
	output logic [3:0] RA1E;
	output logic [3:0] RA2E;

	
	

	always_ff @(negedge clk)begin
	
			Cond_3128_E = Cond_3128_D;
			VALUctlE = VALUctlD;
			VectorE = VectorD;
			Cond_1512_E = Cond_1512_D;
			NoWriteE = NoWriteD;
			PCSrcE = PCSrcD;
			RegWriteE = RegWriteD;
			MemtoRegE = MemtoRegD;
			MemWriteE = MemWriteD;
			ALUControlE = ALUControlD; 
			BranchE = BranchD;
			ALUSrcE = ALUSrcD;
			FlagWriteE = FlagWriteD;
			Flags_out = Flags_in;
			RD1_E = RD1_D;
			RD2_E = RD2_D;
			ExtImmE = ExtImmD;
			RA1E = RA1D;
			RA2E = RA2D;
	
		if(Clear)begin
		
			Cond_3128_E = 4'd0;
			Cond_1512_E = 4'd0;	
			VALUctlE = 4'b0;
			VectorE = 1'b0;
			PCSrcE = 1'd0;
			RegWriteE = 1'd0;
			MemtoRegE = 1'd0;
			MemWriteE = 1'd0;
			ALUControlE = 2'd0; 
			BranchE = 1'd0;
			ALUSrcE = 1'd0;
			FlagWriteE = 2'd0;
			Flags_out = 4'd0;
			RD1_E = 32'd0;
			RD2_E = 32'd0;
			ExtImmE = 32'd0;
			RA1E = 4'b0;
			RA2E = 4'b0; 
		end 
		
	end	

endmodule