//module ARM_Pipeline_processor_top(
module ARM_Pipeline_processor_top(

    //////////// CLOCK //////////
    input                           CLOCK_50,

    //////////// SW //////////
    input             [17:0]        SW,

    input [1:0] KEY,

    //////////// VGA //////////
    output             [7:0]        VGA_B,
    output                          VGA_BLANK_N,
    output                          VGA_CLK,
    output             [7:0]        VGA_G,
    output                          VGA_HS,
    output             [7:0]        VGA_R,
    output                          VGA_SYNC_N,
    output                          VGA_VS,
	  output logic 		   [31:0] 		  ALUResult_c
    //output  logic [31:0] RamOut[100:0]
);

//logic [31:0] RAM_O [3000:0];
//logic [31:0] regOut[14:0];
logic PCSrcW, NoWriteD, NoWriteE ,StallF, StallD, RegWriteW, ALUSrcD, BranchD, MemWriteD, MemtoRegD, RegWriteD, PCSrcD, VectorE, VectorD;
logic PCSrcE, RegWriteE, MemtoRegE, MemWriteE, BranchE, ALUSrcE, MemtoRegWB, PCSrcM, RegWriteM;
logic PCSrc_E, RegWrite_E, MemWrite_E, MemtoRegM, MemWriteM, FlushE;
logic [31:0] ResultW, InstF, InstD, PCPlus4,RD1, RD2, Extend, RD_out, ALUResult_WB, WriteDataE;
logic [31:0] ALUResult_M, WriteDataM, RD, ALUResultE, ExtendE, RD1E, RD2E,fin;
logic [3:0] WA3W, WA3M, FlagsFeedback, CondD, FlagsD, WA3D, CondE, WA3E, FlagsE,RA1D,RA2D,RA1E,RA2E, VALUctlE, VALUctlD;
logic [1:0] ALUCtrD, FlagWriteD, ALUControlE, FlagsWriteE, ForwardAE, ForwardBE;


	logic [7:0] iRed, iGreen, iBlue;

  logic CLK_25;

	// img_printer
  logic [31:0] pixel_value;
  logic oRequest;

  logic [31:0] pix_address;
  logic [9:0] r_out;
  logic [9:0] g_out;
  logic [9:0] b_out;
  logic vga_reset_n;

  logic clk_25;

  logic oVGA_BLANK;
  logic oVGA_SYNC;

  logic [31:0] q_b;

  //memory words
//logic [31:0] ramout[100:0];



Fetch fetch(

.PCSrcW(PCSrcW),
.clk(CLOCK_50),
.reset(~KEY[0]),
.StallF(StallF),
.ResultW(ResultW),
.InstrF(InstF),
.PCPlus4(PCPlus4)

);

pipe_decode  P_decode(

.clk(CLOCK_50),
.Enable(StallD),
.InstF(InstF),
.InstD(InstD)
);


Decode Decode_module
(
.RegWriteW(RegWriteW),
.clk(CLOCK_50),
.reset(~KEY[0]),
.VALUctl(VALUctlD),
.Vector(VectorD),
.WA3W(WA3W),
.RA1D_out(RA1D),
.RA2D_out(RA2D),
.InstrD(InstD),
.PCPlus8D(PCPlus4),
.ResultW(ResultW),
.ALUSrcD(ALUSrcD),
.BranchD(BranchD),
.MemWriteD(MemWriteD),
.MemtoRegD(MemtoRegD),
.RegWriteD(RegWriteD),
.PCSrcD(PCSrcD),
.NoWriteD(NoWriteD),
.ALUCtrD(ALUCtrD),
.FlagWriteD(FlagWriteD),
.CondD(CondD),
.WA3D(WA3D),
.RD1(RD1),
.RD2(RD2),
.Extend(Extend)
//.regOut(regOut)
);


pipe_execute P_execute(
.clk(CLOCK_50),
.Clear(FlushE),
.VectorD(VectorD),
.VALUctlD(VALUctlD),
.VectorE(VectorE),
.VALUctlE(VALUctlE),
.NoWriteD(NoWriteD),
.NoWriteE(NoWriteE),
.Cond_3128_D(CondD),
.Cond_3128_E(CondE),
.Cond_1512_E(WA3E),
.Cond_1512_D(WA3D),
.PCSrcD(PCSrcD),
.RegWriteD(RegWriteD),
.MemtoRegD(MemtoRegD),
.MemWriteD(MemWriteD),
.ALUControlD(ALUCtrD),
.BranchD(BranchD),
.ALUSrcD(ALUSrcD),
.FlagWriteD(FlagWriteD),
.RD1_D(RD1),
.RD2_D(RD2),
.ExtImmD(Extend),
.PCSrcE(PCSrcE),
.Flags_in(FlagsFeedback),
.Flags_out(FlagsE),
.RegWriteE(RegWriteE),
.MemtoRegE(MemtoRegE),
.MemWriteE(MemWriteE),
.ALUControlE(ALUControlE),
.BranchE(BranchE),
.ALUSrcE(ALUSrcE),
.FlagWriteE(FlagsWriteE),
.RD1_E(RD1E),
.RD2_E(RD2E),
.ExtImmE(ExtendE),
.RA1D(RA1D),
.RA2D(RA2D),
.RA1E(RA1E),
.RA2E(RA2E)
);

Execute Execute_module(
.PCSrcE(PCSrcE),
.VALUctlE(VALUctlE),
.VectorE(VectorE),
.RegWriteE(RegWriteE),
.NoWrite(NoWriteE),
.MemWriteE(MemWriteE),
.BranchE(BranchE),
.ALUSrcE(ALUSrcE),
.clk(CLOCK_50),
.reset(~KEY[0]),
.FlagsWriteE(FlagsWriteE),
.ForwardAE(ForwardAE),
.ForwardBE(ForwardBE),
.ALUControlE(ALUControlE),
.CondE(CondE),
.FlagsE(FlagsE),
.RD1E(RD1E),
.RD2E(RD2E),
.ExtendE(ExtendE),
.ResultW(ResultW),
.ALUResultM(ALUResult_M),
.ALUFlagsFeedback(FlagsFeedback),
.PCSrc_E(PCSrc_E),
.RegWrite_E(RegWrite_E),
.MemWrite_E(MemWrite_E),
.ALUResultE(ALUResultE),
.WriteDataE(WriteDataE)
);

pipe_memory P_memory(
.clk(CLOCK_50),
.in_1_E(PCSrc_E),
.in_2_E(RegWrite_E),
.in_3_E(MemtoRegE),
.in_4_E(MemWrite_E),
.ALUResult_E(ALUResultE),
.WriteDataE(WriteDataE),
.WA3E(WA3E),
.PCSrcM(PCSrcM),
.RegWriteM(RegWriteM),
.MemtoRegM(MemtoRegM),
.MemWriteM(MemWriteM),
.ALUResult_M(ALUResult_M),
.WriteDataM(WriteDataM),
.WA3M(WA3M)
);

VGA_Controller U1(
	.iRed(r_out),
	.iGreen(g_out),
	.iBlue(b_out),
	.oRequest(oRequest),
	.oVGA_R(VGA_R),
	.oVGA_G(VGA_G),
	.oVGA_B(VGA_B),
	.oVGA_H_SYNC(VGA_HS),
	.oVGA_V_SYNC(VGA_VS),
	.oVGA_SYNC(VGA_SYNC_N),
	.oVGA_BLANK(VGA_BLANK_N),
	.oVGA_CLK(VGA_CLK),
	.iCLK(CLK_25),
	.iRST_N(vga_reset_n)
);
	// 	image_printer U0 (
  //   .pixel_value(pixel_value),
  //   .oVGA_H_SYNC(VGA_HS),
  //   .oVGA_V_SYNC(VGA_VS),
  //   .img_selector(SW[16]),
  //   .enable(SW[17]),
  //   .clk(CLOCK_50),
  //   .pix_address(pix_address),
  //   .oRequest(oRequest),
  //   .r_out(r_out),
  //   .g_out(g_out),
  //   .b_out(b_out),
  //   .vga_reset_n(vga_reset_n)
  // );

  ImageReader img_reader (
  	// INPUTS
  	// - same of the vga controller
  	.CLK(CLK_25),
  	// - resets the reader
  	.RST_N(KEY[0]),
  	// - image selector
  	.IMG_SEL(SW[1:0]),

  	// Mem Inputs
  	// - Value read from mem
  	.q_val(q_b),

  	// VGA Controller Inputs
  	// - High when if ready to display
  	.oRequest(oRequest),
  	// - Horizontal singal synchronization
  	.VGA_H_SYNC(VGA_HS),
  	// - Vertical singal synchronization
  	.VGA_V_SYNC(VGA_VS),

  	// OUTPUTS
  	// Mem Outputs
  	.addr(pix_address),

  	// VGA Controller Outputs
  	// - RGB Outputs
  	.R_OUT(r_out),
  	.G_OUT(g_out),
  	.B_OUT(b_out),
  	// - VGA Controller Reset
  	.VGA_RST_N(vga_reset_n)

  );

/*dmem DataMemory(

.clk(CLOCK_50),
.we(MemWriteM),
.a(ALUResult_M),
.wd(WriteDataM),
.rd(RD),
.AddressToRead(pix_address),
.ReadDirection(pixel_value),
.RAM_O(ramout)
);*/

// ram2ports ramMemory(
// 	.address_a(WriteDataM),
// 	.address_b(pix_address),
// 	.clock_a(CLOCK_50),
// 	.clock_b(CLK_25),
// 	.data_a(ALUResult_M),
// 	.data_b(),
// 	.wren_a(MemWriteM),
// 	.wren_b(),
// 	.q_a(RD),
// 	.q_b(q_b)
// );

ram_2_ports ramMemory (
	.address_a(WriteDataM),
	.address_b(pix_address),
	.clock_a(CLOCK_50),
	.clock_b(CLK_25),
	.data_a(ALUResult_M),
	.data_b(),
	.wren_a(MemWriteM),
	.wren_b(),
	.q_a(RD),
	.q_b(q_b)
  );


pipe_write_back P_wb(
  .clk(CLOCK_50),
  .PCSrcM(PCSrcM),
  .RegWriteM(RegWriteM),
  .MemtoRegM(MemtoRegM),
  .RD_in(RD),
  .ALUResult_M(ALUResult_M),
  .WA3M(WA3M),
  .PCSrcWB(PCSrcW),
  .RegWriteWB(RegWriteW),
  .MemtoRegWB(MemtoRegWB),
  .RD_out(RD_out),
  .ALUResult_WB(ALUResult_WB),
  .WA3W(WA3W)
);


mux2 #(.WIDTH(32)) mux21
(
.d0(ALUResult_WB),
.d1(RD_out),
.s(MemtoRegWB),
.y(ResultW)
);


Hazard_Unit Hazard_mdoule(
.RA1E(RA1E),
.RA2E(RA2E),
.WA3M(WA3M),
.WA3W(WA3W),
.RA1D(RA1D),
.RA2D(RA2D),
.WA3E(WA3E),
.RegWriteM(RegWriteM),
.RegWriteW(RegWriteW),
.MemtoRegE(MemtoRegE),
.ForwardAE(ForwardAE),
.ForwardBE(ForwardBE),
.StallF(StallF),
.StallD(StallD),
.FlushE(FlushE)
);

pll_25MHz pll_25 (
	.refclk(CLOCK_50),   //  refclk.clk
	.rst(~KEY[0]),      //   reset.reset
	.outclk_0(CLK_25)  // outclk0.clk
);

assign ALUResult_c = ALUResult_M;
//assign RamOut = ramout;

endmodule
