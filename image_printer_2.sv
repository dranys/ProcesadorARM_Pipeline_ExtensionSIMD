module image_printer_2 (
    // INPUTS
    input logic [31:0] pixel_value,
    input logic oRequest,
    input logic oVGA_H_SYNC,
    input logic oVGA_V_SYNC,
	  input logic [1:0] img_selector,
    input logic enable,
    input logic clk,
    // OUTPUTS
    output logic [31:0] pix_address,
    output logic [7:0] r_out,
    output logic [7:0] g_out,
    output logic [7:0] b_out,
    output logic vga_reset_n
);
// dirección initial para la imagen
parameter img_init_address = 0;
parameter img_second_address = 0;
parameter img_third_address = 0;

parameter dimension = 200;


// contadores de img
logic [31:0] img_v_counter;

logic [31:0] img_h_counter;

// limites para la imagen a la hora de pintarla
logic [31:0] img_screen_height;

logic [31:0] img_screen_width;

logic [1:0] byte_counter;

always @ (posedge clk ) begin

  if (!enable) begin // valores iniciales
    r_out <= 0;
    g_out <= 0;
    b_out <= 0;
    img_h_counter <= 0;
    img_v_counter <= 0;
    pix_address <= img_init_address;
    vga_reset_n = 0;
    img_screen_height = dimension;
    img_screen_width = dimension;
    byte_counter = 0;
  end else begin // se empieza a hacer fetch de la informacion
    vga_reset_n = 1;
    if (img_v_counter < img_screen_height) begin
      if (!oRequest && img_h_counter == 0 ) begin
        // muestra pixel
        r_out = 0;
        g_out = 0;
        b_out = 0;
      end else if (oRequest && img_h_counter < img_screen_width && oVGA_H_SYNC) begin

        if(byte_counter == 2'b00)begin
          r_out <= pixel_value[31:24];
          g_out <= pixel_value[31:24];
          b_out <= pixel_value[31:24];
          byte_counter <= 2'b01;
          img_h_counter <= img_h_counter + 8'h1;
        end
        else if(byte_counter == 2'b01) begin
          r_out = pixel_value[23:16];
          g_out = pixel_value[23:16];
          b_out = pixel_value[23:16];
          byte_counter = 2'b10;
          img_h_counter = img_h_counter + 8'h1;
        end
        else if(byte_counter == 2'b10) begin
          r_out <= pixel_value[15:8];
          g_out <= pixel_value[15:8];
          b_out <= pixel_value[15:8];
          byte_counter <= 2'b11;
          img_h_counter <=img_h_counter + 8'h1;
        end
        else if(byte_counter == 2'b11) begin
          r_out <= pixel_value[7:0];
          g_out <= pixel_value[7:0];
          b_out <= pixel_value[7:0];
          byte_counter <= 2'b00;
          img_h_counter <= img_h_counter + 8'h1;
          // aumenta direccion
          pix_address <=  pix_address + 8'h1;
        end
      end else if (img_h_counter >= img_screen_width && oVGA_H_SYNC) begin
        // muestra pixel
        r_out = 0;
        g_out = 0;
        b_out = 0;
      end  else if (!oVGA_H_SYNC) begin
        // muestra pixel
        r_out = 0;
        g_out = 0;
        b_out = 0;
        // resetea contadores
        img_h_counter = 0;
        img_v_counter = img_v_counter + 1;
      end
    end else if (oVGA_V_SYNC) begin
      r_out = 0;
      g_out = 0;
      b_out = 0;
    end else begin
      //reseto
      r_out = 0;
      g_out = 0;
      b_out = 0;

      img_v_counter = 0;
      img_h_counter = 0;

      byte_counter = 0;

      pix_address = img_init_address;
    end
  end
end

endmodule // image_printer
