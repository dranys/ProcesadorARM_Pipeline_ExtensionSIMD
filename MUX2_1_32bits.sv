module MUX2_1_32bits(

	input logic [31:0] A,B,
	input logic sel,
	output logic [31:0] Mout
    );
	always_comb
	
	case(sel) // selecciono el caso segun sea mi sel
		1'b0: Mout = A;
		1'b1: Mout = B;
	endcase

endmodule