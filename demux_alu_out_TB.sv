 `timescale 1ns / 1ps  

module tb_demux_alu_out;
//Inputs
 logic[31:0] ResultA,ResultB;
 logic[3:0] FlagsA, FlagsB;
 logic Vector;

//Outputs
 logic [31:0] AluResult;
 logic [3:0] Flags;

 demux_alu_out dem(
        .Vector(Vector),
        .AluResultA(ResultA),
        .AluResultB(ResultB),
        .Vflags(FlagsB),
        .flags(FlagsA),
        .AluResult(AluResult),
        .AluFlags(Flags)
        
     );

    initial begin
    // hold reset state for 100 ns.
      ResultA = 32'heeeeeeee;
      ResultB = 32'haaaaaaaa;
      FlagsA = 4'ha;
      FlagsB = 4'hb;

      Vector = 1'b0;
    end

    always begin

      #20;
      Vector = 1'b1;
      #20;
      Vector = 1'b0;

    end
endmodule