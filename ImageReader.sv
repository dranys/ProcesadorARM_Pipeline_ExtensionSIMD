module ImageReader (
	// INPUTS
	// - same of the vga controller
	input logic CLK,
	// - resets the reader
	input logic RST_N,
	// - image selector
	input logic [1:0] IMG_SEL,

	// Mem Inputs
	// - Value read from mem
	input logic [31:0] q_val,

	// VGA Controller Inputs
	// - High when if ready to display
	input logic oRequest,
	// - Horizontal singal synchronization
	input logic VGA_H_SYNC,
	// - Vertical singal synchronization
	input logic VGA_V_SYNC,

	// OUTPUTS
	// Mem Outputs
	output logic [31:0] addr,

	// VGA Controller Outputs
	// - RGB Outputs
	output logic [7:0] R_OUT,
	output logic [7:0] G_OUT,
	output logic [7:0] B_OUT,
	// - VGA Controller Reset
	output logic VGA_RST_N

);

// Mem direction for the original image
parameter IMG_ADDR_BEF = 8;
// Mem direction for the compressed image
parameter IMG_ADDR_AFT = 10009;
// mem direction for the uncompressed image
paramter UMC_ADDR = 20009;
// Height and width of the image
parameter IMG_DIMENSION = 200;
// Total amount of pixeles per image
//parameter TOTAL_PIX = 90000;

// counters for vertical and horizonta pixels
logic [31:0] img_h_counter;
logic [31:0] img_v_counter;
// counter for pixels in a 32 bit register
logic [1:0] eight_bit_pix_counter;

logic inside_valid_flag;
logic next_line_flag;

logic [31:0] q_val_tmp;

assign q_val_tmp = (q_val);


initial begin
	R_OUT = 0;
	G_OUT = 0;
	B_OUT = 0;

	img_v_counter = 0;
	img_h_counter = 0;

	// Takes the compressed image or the original image
	if(IMG_SEL == 0) begin
		addr = IMG_ADDR_AFT;
	end
	else if (IMG_SEL == 1) begin
		addr = IMG_ADDR_BEF;
	end
	else if (IMG_SEL == 2) begin
		addr = UMC_ADDR;
	end

	inside_valid_flag = 0;
	next_line_flag = 0;

	eight_bit_pix_counter = 0;

	VGA_RST_N = 0;
end


always @(posedge CLK or negedge RST_N) begin

	// RST_N negated
	if(!RST_N) begin
		R_OUT <= 0;
		G_OUT <= 0;
		B_OUT <= 0;
		// Takes the compressed image or the original image
		if(IMG_SEL == 0) begin
			addr = IMG_ADDR_AFT;
		end
		else if (IMG_SEL == 1) begin
			addr = IMG_ADDR_BEF;
		end
		else if (IMG_SEL == 2) begin
			addr = UMC_ADDR;
		end
		// Keeps the VGA Controller off
		VGA_RST_N = 0;

		img_v_counter <= 0;
		img_h_counter <= 0;

		eight_bit_pix_counter <= 0;
	end

	else begin
		VGA_RST_N = 1;

		if (VGA_V_SYNC) begin
			//inside of a frame
			if (VGA_H_SYNC) begin
				//inside of a row
				if (oRequest) begin
					next_line_flag <= 1;
					inside_valid_flag <= 1;
					//inside of valid part
					if(img_v_counter < IMG_DIMENSION) begin
						// inside vertical boundaries of de image
						if (img_h_counter < IMG_DIMENSION) begin
							// inside horizontal boundaries of the image

							// validates the pixel in the 32 bit register
							case(eight_bit_pix_counter)
								2'b00: begin
									R_OUT <= q_val_tmp[7:0];
									G_OUT <= q_val_tmp[7:0];
									B_OUT <= q_val_tmp[7:0];
									eight_bit_pix_counter <= eight_bit_pix_counter + 1'b1;
									img_h_counter <= img_h_counter + 1;
								end
								2'b01: begin
									R_OUT <= q_val_tmp[15:8];
									G_OUT <= q_val_tmp[15:8];
									B_OUT <= q_val_tmp[15:8];
									eight_bit_pix_counter <= eight_bit_pix_counter + 1'b1;
									img_h_counter <= img_h_counter + 1;
								end
								2'b10: begin
									R_OUT <= q_val_tmp[23:16];
									G_OUT <= q_val_tmp[23:16];
									B_OUT <= q_val_tmp[23:16];
									eight_bit_pix_counter <= eight_bit_pix_counter + 1'b1;
									img_h_counter <= img_h_counter + 1;
								end
								2'b11: begin
									R_OUT <= q_val_tmp[31:24];
									G_OUT <= q_val_tmp[31:24];
									B_OUT <= q_val_tmp[31:24];
									eight_bit_pix_counter <= 2'b00;
									img_h_counter <= img_h_counter + 1;
									addr <= addr + 1;
								end
							endcase
						end // if img_h_counter < IMG_DIMENSION
						else begin
							R_OUT <= 0;
							G_OUT <= 0;
							B_OUT <= 0;
						end
					end // if img_v_counter < IMG_DIMENSION
					else begin
						R_OUT <= 0;
						G_OUT <= 0;
						B_OUT <= 0;
					end
				end // if oRequest
				else begin
					R_OUT <= 0;
					G_OUT <= 0;
					B_OUT <= 0;
				end
			end // if VGA_H_SYNC
			else begin
				if(next_line_flag) begin
					next_line_flag = 0;
					if(inside_valid_flag) begin
						img_v_counter <= img_v_counter + 1;
						img_h_counter <= 0;
					end
				end
			end
		end // if de VGA_V_SYNC
		else begin
			// end of frame
			R_OUT <= 0;
			G_OUT <= 0;
			B_OUT <= 0;

			inside_valid_flag <= 0;
			next_line_flag <= 0;

			img_v_counter <= 0;
			img_h_counter <= 0;

			eight_bit_pix_counter <= 0;

			if(IMG_SEL == 0) begin
				addr = IMG_ADDR_AFT;
			end
			else if (IMG_SEL == 1) begin
				addr = IMG_ADDR_BEF;
			end
			else if (IMG_SEL == 2) begin
				addr = UMC_ADDR;
			end
		end
	end
end // end of always

endmodule
