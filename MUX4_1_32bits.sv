module MUX4_1_32bits(//A 00, B 01, C 10, D 11

	input logic [31:0] A,B, C, D,
	input logic [1:0] sel,
	output logic [31:0] Mout
    );
	 
	 
	always_comb
	case(sel) // selecciono el caso segun sea mi sel
		2'b00: Mout = A;
		2'b01: Mout = B;
		2'b10: Mout = C;
		2'b11: Mout = D;
	endcase

endmodule